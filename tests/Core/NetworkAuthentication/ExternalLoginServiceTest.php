<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Tests\Core\NetworkAuthentication;

use App\Core\NetworkAuthentication\ConnectedAccount;
use App\Core\NetworkAuthentication\ConnectedAccountRepository;
use App\Core\NetworkAuthentication\Domain\ProviderReference;
use App\Core\NetworkAuthentication\Exception\UnknownOrganisation;
use App\Core\NetworkAuthentication\Exception\WrongProvider;
use App\Core\NetworkAuthentication\ExternalLoginService;
use App\Core\NetworkAuthentication\OpenIdUserInfoUser;
use App\Core\Organisation\Organisation;
use App\Core\Organisation\OrganisationAuthenticationProvider;
use App\Core\Organisation\OrganisationRepository;
use App\Core\Shared\Domain;
use App\Core\User\User;
use App\Core\User\UserRepository;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Core\NetworkAuthentication\ExternalLoginService
 */
class ExternalLoginServiceTest extends TestCase
{

    public function testResolvesExternalUserToExistingUsers()
    {
        $externalUser = new OpenIdUserInfoUser(
            '',
            [
                  'given_name' => 'Jane',
                  'family_name' => 'Doe',
                  'sub' => '0123456789',
                  'email' => 'janedoe@example.com'
              ]
        );

        $user = new User();
        $user->setEmail('janedoe@example.com');
        $connectedProviderRef = new ProviderReference(true, 'actual_provider');
        $connectedAccount = new ConnectedAccount(
            $user,
            $connectedProviderRef,
            '0123456789',
            'janedoe@example.com'
        );
        $connectedAccRepoMock = $this->createMock(ConnectedAccountRepository::class);
        $connectedAccRepoMock->method('getFor')
                             ->with($externalUser)
                             ->willReturn($connectedAccount);

        // Should not be needed if everything goes well
        $userRepositoryMock = $this->createMock(UserRepository::class);
        $organisationRepoMock = $this->createMock(OrganisationRepository::class);

        $usedProvider = new ProviderReference(true, 'actual_provider');

        $sut = new ExternalLoginService($organisationRepoMock, $connectedAccRepoMock, $userRepositoryMock);
        $res = $sut->getUser($externalUser, $usedProvider);

        $this->assertEquals($user, $res);
        $this->assertEquals('janedoe@example.com', $res->getEmail());
    }

    public function testFailsIfOrganisationIsUnkown()
    {
        $this->expectException(UnknownOrganisation::class);

        $organisationRepoMock = $this->createMock(OrganisationRepository::class);
        $connectedAccRepoMock = $this->createMock(ConnectedAccountRepository::class);
        $userRepositoryMock = $this->createMock(UserRepository::class);

        $externalUser = new OpenIdUserInfoUser(
            '',
            [
                  'given_name' => 'Jane',
                  'family_name' => 'Doe',
                  'sub' => '0123456789',
                  'email' => 'janedoe@example.com'
              ]
        );
        $provider = new ProviderReference(true, 'notrelevant');

        $sut = new ExternalLoginService($organisationRepoMock, $connectedAccRepoMock, $userRepositoryMock);
        $sut->getUser($externalUser, $provider);
    }

    public function testFailsIfProviderIsInvalidForOrganisation()
    {
        $this->expectException(WrongProvider::class);

        $organisation1 = $this->createMockOrganisation();
        $organisationRepoMock = $this->createMock(OrganisationRepository::class);
        $organisationRepoMock->method('withDomain')
                             ->with(Domain::fromString('example.com'))
                             ->willReturn($organisation1);

        $connectedAccRepoMock = $this->createMock(ConnectedAccountRepository::class);
        $userRepositoryMock = $this->createMock(UserRepository::class);

        $externalUser = new OpenIdUserInfoUser(
            '',
            [
                  'given_name' => 'Jane',
                  'family_name' => 'Doe',
                  'sub' => '0123456789',
                  'email' => 'janedoe@example.com'
              ]
        );
        $provider = new ProviderReference(true, 'other_provider');

        $sut = new ExternalLoginService($organisationRepoMock, $connectedAccRepoMock, $userRepositoryMock);
        $sut->getUser($externalUser, $provider);
    }

    /**
     * @return Organisation
     */
    private function createMockOrganisation(): Organisation
    {
        $organisation1 = $this->getMockBuilder(Organisation::class)->setMethods(['getId'])->getMock();
        $organisation1->method('getId')->willReturn('01234567891012');
        $provider1 = new OrganisationAuthenticationProvider();
        $provider1->setDomain('example.com');
        $provider1->setType(OrganisationAuthenticationProvider::TYPE_GLOBAL);
        $provider1->setConfiguration([OrganisationAuthenticationProvider::CONFIG_REFERENCE => 'actual_provider']);
        $organisation1->addProvider($provider1);
        $provider2 = new OrganisationAuthenticationProvider();
        $provider2->setDomain('example.com');
        $provider2->setType(OrganisationAuthenticationProvider::TYPE_GLOBAL);
        $provider2->setConfiguration([OrganisationAuthenticationProvider::CONFIG_REFERENCE => 'actual_provider_2']);
        $organisation1->addProvider($provider1);
        return $organisation1;
    }

    public function testFailsIfExternalAccountNotConnectedToUser()
    {
        $this->expectException(WrongProvider::class);

        // when requested is made with
        $provider = new ProviderReference(true, 'actual_provider_2');
        $externalUser = new OpenIdUserInfoUser(
            '',
            [
                  'given_name' => 'Jane',
                  'family_name' => 'Doe',
                  'sub' => '0123456789',
                  'email' => 'janedoe@example.com'
              ]
        );

        // and no connected account can be found
        $connectedAccRepoMock = $this->createMock(ConnectedAccountRepository::class);
        $connectedAccRepoMock->method('getFor')
                             ->with($externalUser)
                             ->willReturn(null);

        // but organisation exists
        $organisation1 = $this->createMockOrganisation();
        $organisationRepoMock = $this->createMock(OrganisationRepository::class);
        $organisationRepoMock->method('withDomain')
                             ->with(Domain::fromString('example.com'))
                             ->willReturn($organisation1);

        // and user exists with different provider
        $user = new User();
        $connectedProviderRef = new ProviderReference(true, 'actual_provider');
        $user->addConnectedAccount($connectedProviderRef, '0123456789', 'janedoe@example.com');
        $userRepositoryMock = $this->createMock(UserRepository::class);
        $userRepositoryMock->method('withEmail')
                             ->with('janedoe@example.com')
                             ->willReturn($user);

        // this should fail
        $sut = new ExternalLoginService($organisationRepoMock, $connectedAccRepoMock, $userRepositoryMock);
        $sut->getUser($externalUser, $provider);
    }

    public function testCreatesNewUserAccountIfNonExistent()
    {
        $this->expectException(WrongProvider::class);

        // when requested is made with
        $provider = new ProviderReference(true, 'actual_provider_2');
        $externalUser = new OpenIdUserInfoUser(
            '',
            [
                  'given_name' => 'Jane',
                  'family_name' => 'Doe',
                  'sub' => '0123456789',
                  'email' => 'janedoe@example.com'
              ]
        );

        // and no connected account can be found
        $connectedAccRepoMock = $this->createMock(ConnectedAccountRepository::class);
        $connectedAccRepoMock->method('getFor')
                             ->with($externalUser)
                             ->willReturn(null);

        // but organisation exists
        $organisation1 = $this->createMockOrganisation();
        $organisationRepoMock = $this->createMock(OrganisationRepository::class);
        $organisationRepoMock->method('withDomain')
                             ->with(Domain::fromString('example.com'))
                             ->willReturn($organisation1);

        // and user does not exist at all
        $userRepositoryMock = $this->createMock(UserRepository::class);
        $userRepositoryMock->method('withEmail')
                           ->with('janedoe@example.com')
                           ->willReturn(null);

        $sut = new ExternalLoginService($organisationRepoMock, $connectedAccRepoMock, $userRepositoryMock);
        $user = $sut->getUser($externalUser, $provider);

        $this->assertEquals('01234567891012', $user->getOrganisation()->getId());
        $this->assertEquals('Jane', $user->getGivenName());
        $this->assertEquals('Doe', $user->getFamilyName());
        $this->assertEquals('janedoe@example.com', $user->getEmail());
    }
}
