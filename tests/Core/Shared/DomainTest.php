<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Tests\Core\Shared;

use App\Core\Shared\Domain;
use PHPUnit\Framework\TestCase;

class DomainTest extends TestCase
{
    const VALID_DOMAINS = [
        'example.com',
        'mitgliederportal.org',
        'xxx.yz'
    ];

    const VALID_EMAILS = [
        'mail@example.com',
        'abc123@mitgliederportal.org',
        '99.zdf@xxx.yz'
    ];

    public function testDoesNotAcceptInvalidDomains()
    {
        $this->expectException(\InvalidArgumentException::class);
        Domain::fromString('');
    }

    public function testCanBeConvertedToString()
    {
        $domain = Domain::fromString('example.com');
        $this->assertEquals('Domain is example.com', 'Domain is ' . $domain);
    }

    public function testCanBeCreatedFromEmail()
    {
        foreach (self::VALID_EMAILS as $index => $EMAIL) {
            $domain = Domain::fromEmail($EMAIL);
            $this->assertInstanceOf(Domain::class, $domain);
            $this->assertEquals(self::VALID_DOMAINS[$index], $domain->value());
        }
    }

    public function testCanBeCreatedFromString()
    {
        foreach (self::VALID_DOMAINS as $DOMAIN) {
            $domain = Domain::fromString($DOMAIN);
            $this->assertInstanceOf(Domain::class, $domain);
            $this->assertEquals($DOMAIN, $domain->value());
        }
    }
}
