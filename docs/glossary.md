# Glossary
by Domains


## Network Authentication

### Provider
A provider is a configuration object defining one external authentication service that can be used to sign-in users.

### Global Provider
These providers are configured by the system administrator and 
are available to all organisations and for auto discovery of organisations.
The domain of the external users email is mapped to the organisations domain.

Each organisation needs to enable the global provider(s) they want to use.

### Client
A specific implementation for an authentication service that recieves a provider configuration and then manages the communication with the service.