# Setup

After following the installation instructions on the ReadMe page, the following steps have to be taken to get started:

# Configure the System
All base configuration of the system is done through the user-config folder.
To get started copy the local.yaml.dist or local-min.yaml.dist file to local.yaml and adapt the values.

# Generate a first (local) admin account
1. Run `php bin/console security:encode-password` and temporary save the encoded password
2. Run `php app:user:create --admin <email> <given_name> <family_name> <encryptedPassword>`
3. Log in with the new account.

Now you can configure the organisation etc. through the administration area.

# Design
The images in the /public/img folder can be replaced, but any changes here may be overwritten by an update.

