# Authorization Code Flow

Developer Documentation

## Network Authentication

The Authorization Code Flow is initiated by a request to the OAuth2 Authorization endpoint.

```http request
POST {{baseurl}}/oauth2/authorize
?response_type=code
&client_id=1e4e8e59-64d7-11eb-b478-0242ac150002
&scope=organisation.read-only
&redirect_uri=https://oauthdebugger.com/debug
&state=MY-CSRF-TOKEN
Content-Type: application/x-www-form-urlencoded
```

The Symfony Security System redirects the user to a login page in case they are not logged in already. After the login the user is redirected back to this url.

This request is then handled by the OAuthControllers authorizeAction() method.
The method checks the client and adds the user to the request, which is completed by the OAuthService.


The response will redirect the user the original application attaching a code query param.
This code can be used to make a request to the token endpoint:

````http request
POST {{baseurl}}/oauth2/token
Content-Type: application/x-www-form-urlencoded

grant_type=authorization_code&code={{code}}&client_id=123b9a29-a799-11eb-80e9-0242ac140002&client_secret=1bbcb82df4ecd6fb7e6945d2999759748394e8afaed6ff7f25577901c381b002&redirect_uri=https://oauthdebugger.com/debug&scope=organisation.read-only%20openid%20profile%20email
````

The response should contain an Access Token, eventually a refreshToken and an ID Token (if openid scope requested).
