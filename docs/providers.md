# Providers

Providers connect an identity provider with an organisation and its domain.

An organisation that uses Microsoft for its account management can use the Microsoft provider to enable all users with an email of its organisation to login with their organisations Microsoft account.
Each organisation in the system can have multiple and different providers from others.

# Global
By adding a provider of type Global an organisation can enable a globally configured provider for one of its domains.
Globally configured providers can use any of the below providers, but are configured in advance by the system administrator.

## Local / Network Account
A special option for the global provider is to use the key 'local'. This enables the organisation to use locally created accounts and not rely on external identity provider.

# Google 
Enables registration of the services a client to Googles OAuth System and logins with all Gmail accounts.

Required: Client-ID, Secret
Optional: Hosted Domain

# Microsoft
Enables the usage with the microsoft identity platform.

Required: Client-ID, Secret

# Need something else?
Create an issue and ideally a pull request.