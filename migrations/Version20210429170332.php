<?php
/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210429170332 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Inital database creation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE application (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(180) NOT NULL, secret VARCHAR(180) NOT NULL, organisation CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', allow_access_for_sub_organisations TINYINT(1) NOT NULL, redirect_uris LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', homepage_url VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE connected_account (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', external_id VARCHAR(255) NOT NULL, external_username VARCHAR(255) NOT NULL, provider_is_global TINYINT(1) NOT NULL, provider_key_or_id VARCHAR(255) NOT NULL, INDEX IDX_EEC6115DA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth2_accesstoken (id VARCHAR(80) NOT NULL, expiry_date_time DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', client CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', scopes LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth2_authcode (id VARCHAR(80) NOT NULL, expiry_date_time DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', client CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', scopes LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', user CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth2_refreshtoken (id VARCHAR(80) NOT NULL, expiry_date_time DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', access_token_id VARCHAR(80) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organisation (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', part_of_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', name VARCHAR(255) NOT NULL, abbreviation VARCHAR(50) DEFAULT NULL, contact VARCHAR(255) DEFAULT NULL, INDEX IDX_E6E132B4C97EF49F (part_of_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organisation_authentication_provider (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', organisation_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', type VARCHAR(50) NOT NULL, name VARCHAR(255) NOT NULL, domain VARCHAR(255) NOT NULL, configuration LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', INDEX IDX_D885EADA9E6B1585 (organisation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', organisation_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) DEFAULT NULL, given_name VARCHAR(255) NOT NULL, family_name VARCHAR(255) NOT NULL, is_verified TINYINT(1) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:utc_immutable_datetime)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:utc_immutable_datetime)\', UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), INDEX IDX_8D93D6499E6B1585 (organisation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE connected_account ADD CONSTRAINT FK_EEC6115DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE organisation ADD CONSTRAINT FK_E6E132B4C97EF49F FOREIGN KEY (part_of_id) REFERENCES organisation (id)');
        $this->addSql('ALTER TABLE organisation_authentication_provider ADD CONSTRAINT FK_D885EADA9E6B1585 FOREIGN KEY (organisation_id) REFERENCES organisation (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6499E6B1585 FOREIGN KEY (organisation_id) REFERENCES organisation (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE organisation DROP FOREIGN KEY FK_E6E132B4C97EF49F');
        $this->addSql('ALTER TABLE organisation_authentication_provider DROP FOREIGN KEY FK_D885EADA9E6B1585');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6499E6B1585');
        $this->addSql('ALTER TABLE connected_account DROP FOREIGN KEY FK_EEC6115DA76ED395');
        $this->addSql('DROP TABLE application');
        $this->addSql('DROP TABLE connected_account');
        $this->addSql('DROP TABLE oauth2_accesstoken');
        $this->addSql('DROP TABLE oauth2_authcode');
        $this->addSql('DROP TABLE oauth2_refreshtoken');
        $this->addSql('DROP TABLE organisation');
        $this->addSql('DROP TABLE organisation_authentication_provider');
        $this->addSql('DROP TABLE user');
    }
}
