# Common Auth Service

This service is build to simplify user authorization across very complex, separate organisations structures, like those
found for umbrella organisations. It allows combining different authorization endpoints, for example from different
local organisations, into one central single-sign-on service.

This service, through OAuth can than be used to power other applications, reducing account burden for the end users.

## Requirements & Installation

- PHP >7.4 (>=8 advisable)
- Node LTS & npm (To build assets, not strictly required at runtime)  
- composer 2  
- MariaDB/MySQL Database
- SSH/Terminal access (for installation or running cli commands)

1. Transfer files to or do a git checkout or the repository on your target machine. 
2. Generate secrets/keys, see: https://oauth2.thephpleague.com/installation/  
3. Set configuration (Database connection, App secret & previously generated keys through .env.local file or environment variables).
4. Run `composer install`
5. Create database schema: run `doctrine:migrations:migrate`
6. Build frontend assets: run `./node_modules/.bin/encore production`

To update, update the files or run git pull and then continue with step 4.

Overall you can set up your configuration files and build everything locally and only transfer the final files to your production server, which means no node.js has to be installed.

## Features

This service supports a range of features to consolidate 
different user provider into one central service to power a 
set of applications for umbrella organisations.

- Create organisation hierarchy (Allows national organisations to manage local entities, when service is set up by
  international umbrella organisation)
- Supports external users from: Google, Microsoft
- Local Accounts (for adding users without an external backend)
- Supports OpenIdConnect/OAuth2.0 grants: Auth Code, Refresh Token & Client Credentials for Single-Sign-On
- Add and remove client applications for your organisation and child organisations.
- Use global providers and map users to organisations or 
- Manage different organisations and their custom login providers
- Manager user roles
- Discovery page to let users find the right way to log in

## User Flow

````
User (OrgA) --opens-> 'Umbrella Intranet Portal' --redirects-> CommonAuthService -->redirects-> OrgA Login Provider 
````

## Roadmap

- WIP: Add generic OpenId Connect Login Provider
- Write more (functional) tests 
- Adding cron job feature to clean up expired tokens  
- Reach production stability (no specific issues' known, but not sufficiently tested)
- Easier UI-Customization: Colors, Icon, App Title
- Full OpenID Connect Support (Claims, Endpoints, Errors)  
- User Statistics
- Support Nextcloud as external user provider

## Misc

### Contribution

Your contribution is welcome, please create an issue for any problems and suggestions.

### Development

Repo contains a docker-compose file to start a database.
Uses phpunit for tests.

#### Tooling
For active development, these tools need to be installed (and available in the console) to run the composer scripts.

 - phpmetrics
 - phploc
 - phpcs
 - phpcbf
 - phpunit

All reports are generated in the ./dist folder.

#### IT Things Roadmap
- Set up CI pipeline
- Dependency Checks
- Doctrine Repositories in Infrastructure
- Database error handling

### Thanks

Proudly build with PHP based on Symfony, Doctrine and PHP League packages. Using TailwindCSS for styling.
