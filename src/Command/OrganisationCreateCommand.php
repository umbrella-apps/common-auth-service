<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Command;

use App\Core\Organisation\Organisation;
use App\Core\Organisation\OrganisationRepository;
use App\Core\Shared\Domain;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class OrganisationCreateCommand extends Command
{
    protected static $defaultName = 'app:organisation:create';

    /**
     * @var OrganisationRepository
     */
    private $organisationRepository;

    public function __construct(OrganisationRepository $repository)
    {
        parent::__construct();
        $this->organisationRepository = $repository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Create a new organisation')
            ->addArgument('name', InputArgument::REQUIRED)
            ->addArgument('domain', InputArgument::REQUIRED)
            ->addArgument('abbreviation', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $domain = Domain::fromString($input->getArgument('domain'));
        $organisation = Organisation::newFromDomain($domain, 'local');
        $organisation->setName($input->getArgument('name'));
        $organisation->setAbbreviation($input->getArgument('abbreviation'));

        $this->organisationRepository->persist($organisation);

        $io->success('Organisation created!');
        return Command::SUCCESS;
    }
}
