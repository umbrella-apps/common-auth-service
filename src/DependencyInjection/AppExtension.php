<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\DependencyInjection;

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class AppExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new AppConfiguration();
        try {
            $config = $this->processConfiguration($configuration, $configs);
        } catch (InvalidConfigurationException $e) {
            trigger_error('Found invalid "app" configuration: ' . $e->getMessage());
            throw $e;
        }

        $container->setParameter('app.email', $config['email']);
        $container->setParameter('app.domain', $config['domain']);
        $container->setParameter('app.external-login.global-providers', $config['external_login']['global_providers']);
    }

    public function getNamespace()
    {
        return 'app';
    }

    public function getAlias()
    {
        return 'app';
    }
}
