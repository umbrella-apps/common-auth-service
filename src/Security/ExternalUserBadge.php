<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Security;

use App\Core\NetworkAuthentication\ExternalUser;
use App\Core\NetworkAuthentication\Domain\ProviderReference;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\BadgeInterface;
use Symfony\Component\Security\Http\EventListener\UserProviderListener;

class ExternalUserBadge implements BadgeInterface
{
    private $externalUser;
    private $userLoader;
    private $user;
    /**
     * @var ProviderReference
     */
    private $providerReference;

    public function __construct(
        ExternalUser $userIdentifier,
        ProviderReference $providerReference,
        ?callable $userLoader = null
    ) {
        $this->externalUser = $userIdentifier;
        $this->userLoader = $userLoader;
        $this->providerReference = $providerReference;
    }

    public function getExternalUser(): string
    {
        return $this->externalUser;
    }

    public function getUser(): UserInterface
    {
        if (null === $this->user) {
            if (null === $this->userLoader) {
                throw new \LogicException(
                    sprintf(
                        'No user loader is configured, did you forget to register the "%s" listener?',
                        UserProviderListener::class
                    )
                );
            }

            $this->user = ($this->userLoader)($this->externalUser, $this->providerReference);
            if (!$this->user instanceof UserInterface) {
                throw new UsernameNotFoundException();
            }
        }

        return $this->user;
    }

    public function isResolved(): bool
    {
        return true;
    }
}
