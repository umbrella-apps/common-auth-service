<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Security;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class TokenAuthenticatedToken extends AbstractToken
{
    private $accessToken;

    public function __construct($accessToken, $user, array $scopes)
    {
        parent::__construct($scopes);
        $this->setUser($user);
        $this->accessToken = $accessToken;
    }

    public function getCredentials()
    {
        return $this->accessToken;
    }
}
