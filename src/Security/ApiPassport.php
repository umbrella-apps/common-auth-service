<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Security;

use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportTrait;

final class ApiPassport implements PassportInterface
{
    use PassportTrait;



    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string[]
     */
    private $scopes;

    /**
     * @var string
     */
    private $sub;

    /**
     * ApiPassport constructor.
     * @param string $accessToken
     * @param string[] $scopes
     * @param string $sub
     */
    public function __construct(string $accessToken, array $scopes, string $sub)
    {
        $this->accessToken = $accessToken;
        $this->scopes = $scopes;
        $this->sub = $sub;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @return string[]
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }

    /**
     * @return string
     */
    public function getSub(): string
    {
        return $this->sub;
    }
}
