<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Security;

use App\Core\NetworkAuthentication\Client;
use App\Core\NetworkAuthentication\ExternalLoginService;
use App\Core\NetworkAuthentication\ExternalUser;
use App\Core\NetworkAuthentication\OAuth2Client;
use App\Core\NetworkAuthentication\ProviderClientServiceImpl;
use App\Core\NetworkAuthentication\Domain\ProviderReference;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * This authenticator covers the /check routes for OAuth clients,
 * to complete any AuthCode grant processes for external logins.
 */
class OAuth2AuthCodeAuthenticator extends AbstractAuthenticator
{
    use TargetPathTrait;

    const ROUTE_REGEXP = '/^\/connect\/(g\/)?([a-z0-9-_]+)\/check$/';
    const REQUEST_DETAILS_PROVIDER = 'provider';
    const REQUEST_DETAILS_AUTHCODE = 'code';
    const GRANT_NAME = 'authorization_code';
    const REQUEST_METHOD = 'GET';

    /**
     * @var ExternalLoginService
     */
    private $externalLoginService;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var ProviderClientServiceImpl
     */
    private $clientService;

    public function __construct(
        ProviderClientServiceImpl $clientService,
        ExternalLoginService $externalLoginService,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->externalLoginService = $externalLoginService;
        $this->urlGenerator = $urlGenerator;
        $this->clientService = $clientService;
    }

    public function supports(Request $request): bool
    {
        if (!$request->isMethod(self::REQUEST_METHOD)) {
            return false;
        }

        $providerReference = $this->containsProviderDetails($request);
        return $providerReference
            && $this->getClient($providerReference) instanceof OAuth2Client;
    }

    /**
     * returns either false or the provider details
     * @param Request $request
     * @return ProviderReference|false
     */
    private function containsProviderDetails(Request $request)
    {
        $pathVariables = [];
        if (
            preg_match(
                self::ROUTE_REGEXP,
                $request->getPathInfo(),
                $pathVariables
            ) !== 1
        ) {
            return false;
        }

        return new ProviderReference($pathVariables[1] === 'g/', $pathVariables[2]);
    }

    private function getClient(ProviderReference $provider): Client
    {
        return $provider->isGlobal() ?
            $this->clientService->getGlobalClient($provider->getKeyOrId()) :
            $this->clientService->getClient($provider->getKeyOrId());
    }

    public function authenticate(Request $request): PassportInterface
    {
        $requestDetails = $this->getRequestDetails($request);
        $externalUser = $this->completeAuthorizationAndGetUser($requestDetails);
        return $this->createPassport(
            $externalUser,
            $requestDetails[self::REQUEST_DETAILS_PROVIDER]
        );
    }

    private function getRequestDetails(Request $request): array
    {
        $providerReference = $this->containsProviderDetails($request);
        $authCode = $request->query->get(self::REQUEST_DETAILS_AUTHCODE);

        if (null === $authCode) {
            // The code header was empty, authentication fails with HTTP Status
            // Code 401 "Unauthorized"
            throw new AuthenticationException();
        }

        return [
            self::REQUEST_DETAILS_PROVIDER => $providerReference,
            self::REQUEST_DETAILS_AUTHCODE => $authCode
        ];
    }

    private function completeAuthorizationAndGetUser(
        array $requestDetails
    ): ExternalUser {
        $client = $this->getClient($requestDetails[self::REQUEST_DETAILS_PROVIDER]);
        $token = $client->getAccessToken(self::GRANT_NAME, $requestDetails);
        return $client->getUserDetailsFor($token);
    }

    private function createPassport(
        ExternalUser $externalUser,
        ProviderReference $providerReference
    ): ExternalUserPassport {
        $user = new ExternalUserBadge(
            $externalUser,
            $providerReference,
            function (
                ExternalUser $externalUser,
                ProviderReference $provider
            ) {
                return $this->externalLoginService->getUser($externalUser, $provider);
            }
        );

        return new ExternalUserPassport($user);
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationSuccess(
        Request $request,
        TokenInterface $token,
        string $firewallName
    ): ?Response {
        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->urlGenerator->generate('home'));
    }

    /**
     * @inheritDoc
     */
    public function onAuthenticationFailure(
        Request $request,
        AuthenticationException $exception
    ): ?Response {
        $data = [
            // TODO you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED); // TODO always JSON?
    }
}
