<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Security;

use App\Core\OAuth\OAuth2ResourceService;
use League\OAuth2\Server\Exception\OAuthServerException;
use Nyholm\Psr7\ServerRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Authenticator\AuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;

class APIAuthenticator implements AuthenticatorInterface, AuthenticationEntryPointInterface
{
    /**
     * @var OAuth2ResourceService
     */
    private $resourceService;

    public function __construct(OAuth2ResourceService $resourceService)
    {
        $this->resourceService = $resourceService;
    }

    public function supports(Request $request): ?bool
    {
        return $request->headers->has('authorization');
    }

    public function authenticate(Request $request): PassportInterface
    {
        // creating a mock request containing the credentials from the original request
        // (this way we can use the PHP League Implementation here, which couldn't make
        // use of the symfony request anyway)
        $psrRequest = new ServerRequest('GET', '/', $request->headers->all());

        try {
            $psrRequest = $this->resourceService->validateAuthenticatedRequest($psrRequest);
            // if it did not throw an exception yet, the token is valid
            $scopes = $psrRequest->getAttribute('oauth_scopes');
            $sub = $psrRequest->getAttribute('oauth_user_id');
            return new ApiPassport((string)$request->headers->get('authorization'), $scopes, $sub);
        } catch (OAuthServerException $e) {
            throw new BadCredentialsException($e->getMessage(), 0, $e);
        }
    }

    public function createAuthenticatedToken(PassportInterface $passport, string $firewallName): TokenInterface
    {
        /** @var ApiPassport $passport */
        return new TokenAuthenticatedToken($passport->getAccessToken(), $passport->getSub(), $passport->getScopes());
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new JsonResponse(['message' => 'authentication failed']);
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new Response('Auth header required', 401);
    }
}
