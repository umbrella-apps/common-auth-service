<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\Application;

use App\Core\Organisation\Organisation;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 */
class Application
{
    const GRANT_CLIENT_CREDENTIALS = 'client_credential';
    const GRANT_AUTHORIZATION_CODE = 'authorization_code';
    const GRANT_REFRESH_TOKEN = 'refresh_token';

    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=180)
     */
    private $secret;

    /**
     * @var string
     * @ORM\Column(type="guid", nullable=true)
     */
    private $organisation;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $allowAccessForSubOrganisations = false;

    /**
     * @var string[]
     * @Assert\Count(min="1")
     * @ORM\Column(type="json")
     */
    private $redirectUris;

    /**
     * The url that users can use to open the app.
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $homepageUrl;

    public function id()
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function isConfidential(): bool
    {
        return true;
    }

    public function secret(): string
    {
        return $this->secret;
    }

    /**
     * @return string[]
     */
    public function getRedirectUris(): array
    {
        return $this->redirectUris;
    }

    /**
     * @param string[] $redirectUris
     */
    public function setRedirectUris(array $redirectUris): void
    {
        $this->redirectUris = array_values($redirectUris);
    }

    public function setSecret(string $secret): void
    {
        $this->secret = $secret;
    }

    public function getOrganisation(): string
    {
        return $this->organisation;
    }

    public function setOrganisation(string $organisation): void
    {
        $this->organisation = $organisation;
    }

    public function isAllowAccessForSubOrganisations(): bool
    {
        return $this->allowAccessForSubOrganisations;
    }

    public function setAllowAccessForSubOrganisations(bool $allowAccessForSubOrganisations): void
    {
        $this->allowAccessForSubOrganisations = $allowAccessForSubOrganisations;
    }

    public function hasAccess(Organisation $organisation): bool
    {
        if ($organisation->getId() === $this->getOrganisation()) {
            return true;
        }

        if (!$this->allowAccessForSubOrganisations) {
            // the client is not actived for client organisations
            // there it does not need to be checked if the client
            // belongs to a parent of the organisation
            return false;
        }

        // client is not from the given organisation, but may belong to a parent one
        if (null !== $parent = $organisation->getPartOf()) {
            return $this->hasAccess($parent);
        }

        return false;
    }

    public function getHomepageUrl(): string
    {
        return $this->homepageUrl;
    }

    public function setHomepageUrl(string $homepageUrl): void
    {
        $this->homepageUrl = $homepageUrl;
    }

    public function allowedGrantTypes(): array
    {
        return [
            self::GRANT_AUTHORIZATION_CODE,
            self::GRANT_REFRESH_TOKEN,
            self::GRANT_CLIENT_CREDENTIALS
        ];
    }
}
