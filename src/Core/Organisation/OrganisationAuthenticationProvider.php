<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Core\Organisation;

use App\Core\NetworkAuthentication\Domain\ProviderReference;
use App\Core\Shared\Domain;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 *
 * Use type 'global' to enable a global provider to be used for this organisation
 */
class OrganisationAuthenticationProvider
{
    const TYPE_PARENT = 'parent';
    const TYPE_LOCAL = 'local';
    const TYPE_GLOBAL = 'global';
    const CONFIG_REFERENCE = 'provider';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @var Organisation
     * @ORM\ManyToOne(targetEntity=Organisation::class, inversedBy="providers")
     */
    private $organisation;

    /**
     * Currently one of 'global,'generic','local','google','microsoft'
     * @ORM\Column(type="string", length=50)
     */
    private $type;

    /**
     * End-user facing name of this authentication method
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $domain;

    /**
     * The actual configuration for the client
     * @var array
     * @ORM\Column(type="json")
     */
    private $configuration = [];

    public function getId(): string
    {
        return $this->id;
    }

    public function getOrganisation(): Organisation
    {
        return $this->organisation;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function setDomain($domain): void
    {
        $this->domain = $domain;
    }

    public function setOrganisation(Organisation $organisation): void
    {
        $this->organisation = $organisation;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function referencesParentProvider(): bool
    {
        return self::TYPE_PARENT === $this->getType();
    }

    public function enablesLocalAuthentication(): bool
    {
        return self::TYPE_LOCAL === $this->getType();
    }

    public function referencesGlobalProvider(): bool
    {
        return self::TYPE_GLOBAL === $this->getType();
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getConfiguration(): array
    {
        return $this->configuration;
    }

    public function setConfiguration(array $configuration): void
    {
        $this->configuration = $configuration;
    }

    public function isConfiguredFor(Domain $domain): bool
    {
        return $this->getDomain() === $domain->value();
    }

    public function matches(
        ProviderReference $targetProvider,
        Domain $domain = null
    ): bool {
        if ($domain && !$this->isConfiguredFor($domain)) {
            return false;
        }

        if ($targetProvider->isGlobal()) {
            return $this->referencesGlobalProvider()
                && $this->getConfiguration(
                )[OrganisationAuthenticationProvider::CONFIG_REFERENCE] === $targetProvider->getKeyOrId();
        }

        return $this->getId() === $targetProvider->getKeyOrId();
    }
}
