<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\Organisation;

use App\Core\NetworkAuthentication\Domain\ProviderReference;
use App\Core\Shared\Domain;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Organisation implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $abbreviation;

    /**
     * @var Organisation
     * @ORM\ManyToOne(inversedBy="childOrganisations",targetEntity="Organisation")
     */
    private $partOf = null;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(mappedBy="partOf",targetEntity="Organisation",cascade={"REMOVE"})
     */
    private $childOrganisations;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(mappedBy="organisation",targetEntity="OrganisationAuthenticationProvider",cascade={"PERSIST","REMOVE"})
     */
    private $providers;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @param Domain $domain will be used as name
     * @param string $providerKey needs to refer to global authenticator
     * @return static
     */
    public static function newFromDomain(
        Domain $domain,
        string $providerKey
    ): self {
        $organisation = new self();
        $organisation->setName((string)$domain);
        $provider = new OrganisationAuthenticationProvider();
        $provider->setName('Global');
        $provider->setType('global');
        $provider->setConfiguration(['provider' => $providerKey]);
        $provider->setDomain($domain);
        $organisation->addProvider($provider);
        return $organisation;
    }

    public function addProvider(OrganisationAuthenticationProvider $provider): void
    {
        if (!$this->providers->contains($provider)) {
            $provider->setOrganisation($this);
            $this->providers->add($provider);
        }
    }

    public function __construct()
    {
        $this->providers = new ArrayCollection();
        $this->childOrganisations = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPartOf(): ?Organisation
    {
        return $this->partOf;
    }

    public function setPartOf(?Organisation $partOf): void
    {
        $this->partOf = $partOf;
    }

    public function getAllChildOrganisations(): array
    {
        $organisations = [];
        /** @var self $child */
        foreach ($this->getChildOrganisations()->toArray() as $child) {
            array_push($organisations, $child, ...$child->getAllChildOrganisations());
        }
        return $organisations;
    }

    public function getChildOrganisations(): Collection
    {
        return $this->childOrganisations;
    }

    public function provider(ProviderReference $reference): ?OrganisationAuthenticationProvider
    {
        return array_filter(
            $this->getProviders(),
            fn($p) => $p->getId() === $reference->getKeyOrId()
        )[0] ?? null;
    }

    public function getProviders(): array
    {
        return $this->providers->toArray();
    }

    public function removeProvider(
        OrganisationAuthenticationProvider $provider
    ): void {
        $this->providers->removeElement($provider);
    }

    public function supportsAuthenticationThrough(
        ProviderReference $targetProvider,
        Domain $domain = null
    ): bool {
        /** @var OrganisationAuthenticationProvider $availableProvider */
        foreach ($this->providers as $availableProvider) {
            if ($availableProvider->matches($targetProvider, $domain)) {
                return true;
            }
        }

        return false;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'abbreviation' => $this->getAbbreviation(),
            'contact' => $this->getContact(),
            'is_part_of' => $this->partOf ? $this->partOf->getId() : null,
            'child_organisation' => array_map(fn($o) => $o->getId(), $this->childOrganisations->toArray()),
        ];
    }

    public function getAbbreviation(): ?string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(?string $abbreviation): self
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }
}
