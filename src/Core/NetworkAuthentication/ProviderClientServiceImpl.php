<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

use App\Core\NetworkAuthentication\Exception\UnknownProvider;
use App\Core\Organisation\OrganisationAuthenticationProvider;
use App\Core\Organisation\OrganisationRepository;

/**
 * Makes available the different clients for all configured providers
 */
class ProviderClientServiceImpl implements ProviderClientService
{
    const REDIRECT_URI_TEMPLATE_GLOBAL = '%s/connect/g/%s/check';
    const REDIRECT_URI_TEMPLATE = "%s/connect/%s/check";

    /**
     * @var array
     */
    private $globalProviderConfig;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var OrganisationRepository
     */
    private $organisationRepository;

    /**
     * @var ClientFactory
     */
    private $clientFactory;

    public function __construct($globalProviderConfig, string $domain, OrganisationRepository $organisationRepository, ClientFactory $clientFactory)
    {
        $this->globalProviderConfig = $globalProviderConfig;
        $this->domain = $domain;
        $this->organisationRepository = $organisationRepository;
        $this->clientFactory = $clientFactory;
    }

    public function getClient(string $providerId): Client
    {
        $providerDefinition = $this->getProviderDefinition($providerId);

        if ($providerDefinition->referencesGlobalProvider()) {
            // this provider actually just enables/aliases a global provider
            $providerKey = $providerDefinition
                ->getConfiguration()[OrganisationAuthenticationProvider::CONFIG_REFERENCE];
            return $this->getGlobalClient($providerKey);
        }

        // create organisation specific client
        $config = $this->clientConfigurationFrom($providerDefinition);
        return $this->clientFactory->createFrom($config);
    }

    public function getGlobalClient(string $providerKey): Client
    {
        $config = $this->getGlobalProviderDefinition($providerKey);
        $config['redirectUri'] = sprintf(
            self::REDIRECT_URI_TEMPLATE_GLOBAL,
            $this->domain,
            $providerKey
        );
        return $this->clientFactory->createFrom($config);
    }

    private function getProviderDefinition(
        string $providerId
    ): OrganisationAuthenticationProvider {
        $providerDefinition = $this->organisationRepository->providerWithId($providerId);
        ;

        if (null === $providerDefinition) {
            throw UnknownProvider::withId($providerId);
        }

        if ($providerDefinition->referencesParentProvider()) {
            // this provider actually just enables/aliases a parents provider
            $providerKey = $providerDefinition
                ->getConfiguration()[OrganisationAuthenticationProvider::CONFIG_REFERENCE];
            $this->getProviderDefinition($providerKey);
        }

        return $providerDefinition;
    }

    private function getGlobalProviderDefinition(string $providerKey): array
    {
        if (!array_key_exists($providerKey, $this->globalProviderConfig)) {
            throw UnknownProvider::globalWithId($providerKey);
        }

        return $this->globalProviderConfig[$providerKey];
    }

    private function clientConfigurationFrom(
        OrganisationAuthenticationProvider $providerDefinition
    ): array {
        $config = $providerDefinition->getConfiguration();
        $config['type'] = $providerDefinition->getType();
        $config['redirectUri'] = sprintf(
            self::REDIRECT_URI_TEMPLATE,
            $this->domain,
            $providerDefinition->getId()
        );
        return $config;
    }
}
