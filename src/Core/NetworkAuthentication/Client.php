<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

/**
 * protocol agnostic base client
 */
interface Client
{
    /**
     * Returns the URL to be called to start the authentication process
     *
     * @return string
     */
    public function getAuthorizationUrl(): string;
}
