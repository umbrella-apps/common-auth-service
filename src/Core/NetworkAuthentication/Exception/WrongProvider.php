<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication\Exception;

use App\Core\NetworkAuthentication\Domain\ProviderReference;
use App\Core\NetworkAuthentication\ExternalUser;
use App\Core\NetworkAuthentication\NetworkAuthenticationException;
use App\Core\Organisation\Organisation;
use App\Core\Shared\Domain;

class WrongProvider extends NetworkAuthenticationException
{
    public static function forOrganisation(Organisation $organisation, Domain $domain, ProviderReference $usedProvider)
    {
        return new self(
            sprintf(
                'Wrong provider (%s) and domain (%s) combination for organisation. Organisation supports only %s',
                $usedProvider,
                $domain,
                join(', ', array_keys($organisation->getProviders()))
            )
        );
    }

    public static function forUser(
        ExternalUser $externalUser
    ) {
        return new self(
            'Username known, but different provider configured.'
        );
    }
}
