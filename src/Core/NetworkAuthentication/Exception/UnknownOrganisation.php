<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication\Exception;

use App\Core\NetworkAuthentication\NetworkAuthenticationException;
use App\Core\Shared\Domain;

class UnknownOrganisation extends NetworkAuthenticationException
{

    public static function forDomain(Domain $domain)
    {
        return new self(sprintf('no organisation with %s known', $domain->value()));
    }
}
