<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * This class builds a reference to a provider configuration
 *
 * This can either point to a global providers key or the id
 * of an organisation specific one.
 *
 * @ORM\Embeddable()
 */
class ProviderReference
{
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isGlobal;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $keyOrId;

    public function __construct(bool $isGlobal, string $keyOrId)
    {
        $this->isGlobal = $isGlobal;
        $this->keyOrId = $keyOrId;
    }

    public function isGlobal(): bool
    {
        return $this->isGlobal;
    }

    public function getKeyOrId(): string
    {
        return $this->keyOrId;
    }

    public function __toString(): string
    {
        return ($this->isGlobal ? '[GLOBAL]' : '') . $this->keyOrId;
    }
}
