<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

use App\Core\NetworkAuthentication\Domain\ProviderReference;
use App\Core\NetworkAuthentication\Exception\UnknownOrganisation;
use App\Core\NetworkAuthentication\Exception\WrongProvider;
use App\Core\Organisation\Organisation;
use App\Core\Organisation\OrganisationRepository;
use App\Core\User\User;
use App\Core\User\UserRepository;
use App\Core\Shared\Domain;

/**
 * This services handles the translation between external
 * and internal users, when authenticating through a
 * Network Authentication Client
 */
class ExternalLoginService
{
    /**
     * @var OrganisationRepository
     */
    private $organisationRepository;

    /**
     * @var ConnectedAccountRepository
     */
    private $connectedAccountRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        OrganisationRepository $organisationRepository,
        ConnectedAccountRepository $connectedAccountRepository,
        UserRepository $userRepository
    ) {
        $this->organisationRepository = $organisationRepository;
        $this->connectedAccountRepository = $connectedAccountRepository;
        $this->userRepository = $userRepository;
    }

    public function getUser(
        ExternalUser $externalUser,
        ProviderReference $provider
    ): User {
        $connectedAccount = $this->connectedAccountRepository->getFor(
            $externalUser,
            $provider
        );

        if (null === $connectedAccount) {
            // User has never logged in with this provider
            return $this->noConnectedAccountFound($externalUser, $provider);
        }

        return $connectedAccount->user();
    }

    /**
     * Check if the user has logged in with a different
     * provider or silently create new account
     *
     * @param ExternalUser $externalUser
     * @return User
     * @throws WrongProvider User account exists, with different provider
     */
    private function noConnectedAccountFound(
        ExternalUser $externalUser,
        ProviderReference $providerDetails
    ): User {
        $this->checkUserNonExistent($externalUser);

        $domain = Domain::fromEmail($externalUser->email());
        $organisation = $this->getOrganisation($domain, $providerDetails);
        return $this->createUser($externalUser, $organisation, $providerDetails);
    }

    /**
     * Check if the user used the wrong provider
     *
     * @param ExternalUser $externalUser
     * @throws WrongProvider User account exists, with different provider
     */
    private function checkUserNonExistent(ExternalUser $externalUser): void
    {
        $user = $this->userRepository->withEmail($externalUser->email());
        if (null !== $user) {
            throw WrongProvider::forUser($externalUser);
        }
    }

    private function getOrganisation(
        Domain $domain,
        ProviderReference $provider
    ): Organisation {
        $organisation = $this->organisationRepository->withDomain($domain);

        if (null === $organisation) {
            throw UnknownOrganisation::forDomain($domain);
        }

        if (!$organisation->supportsAuthenticationThrough($provider, $domain)) {
            throw WrongProvider::forOrganisation(
                $organisation,
                $domain,
                $provider
            );
        }

        return $organisation;
    }

    private function createUser(
        ExternalUser $externalUser,
        Organisation $organisation,
        ProviderReference $providerReference
    ): User {
        $user = new User();
        $user->setCreatedAt(new \DateTimeImmutable(date('Y-m-d H:i:s')))
             ->setOrganisation($organisation)
             ->setEmail($externalUser->email())
             ->setGivenName($externalUser->givenName())
             ->setFamilyName($externalUser->familyName())
             ->addConnectedAccount(
                 $providerReference,
                 $externalUser->id(),
                 $externalUser->email()
             );

        $this->userRepository->save($user);
        return $user;
    }
}
