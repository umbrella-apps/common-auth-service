<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

class OpenIdUserInfoUser implements ExternalUser
{
    /**
     * @var array
     */
    protected $response;

    /**
     * @var string
     */
    private $provider;

    public function __construct(string $provider, array $response)
    {
        $this->provider = $provider;
        $this->response = $response;
    }

    public function id(): ?string
    {
        return $this->response['sub'];
    }

    /**
     * Get preferred first name.
     *
     * @return string|null
     */
    public function givenName(): ?string
    {
        return $this->getResponseValue('given_name');
    }

    /**
     * Get preferred last name.
     *
     * @return string|null
     */
    public function familyName(): ?string
    {
        return $this->getResponseValue('family_name');
    }

    /**
     * Get locale.
     *
     * @return string|null
     */
    public function locale(): ?string
    {
        return $this->getResponseValue('locale');
    }

    /**
     * Get email address.
     *
     * @return string|null
     */
    public function email(): ?string
    {
        return $this->getResponseValue('email');
    }

    /**
     * Get avatar image URL.
     *
     * @return string|null
     */
    public function avatar()
    {
        return $this->getResponseValue('picture');
    }

    private function getResponseValue($key)
    {
        if (array_key_exists($key, $this->response)) {
            return $this->response[$key];
        }
        return null;
    }

    public function provider(): string
    {
        return $this->provider;
    }
}
