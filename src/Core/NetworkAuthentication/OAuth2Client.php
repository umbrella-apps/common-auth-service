<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

use League\OAuth2\Client\Token\AccessToken;

interface OAuth2Client extends Client
{
    public function getUserDetailsFor(AccessToken $token): ExternalUser;
}
