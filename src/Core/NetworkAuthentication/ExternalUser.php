<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

interface ExternalUser
{
    public function id(): ?string;

    /**
     * @deprecated needs to be more specific
     * @return string
     */
    public function provider(): string;

    public function email(): ?string;

    public function familyName(): ?string;

    public function givenName(): ?string;
}
