<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

use App\Core\NetworkAuthentication\Clients\GenericOAuthClient;
use App\Core\NetworkAuthentication\Clients\GenericOAuthProviderConfig;
use App\Core\NetworkAuthentication\Clients\GoogleConnectClient;
use App\Core\NetworkAuthentication\Clients\MicrosoftConnectClient;

class ClientFactory
{
    /**
     * Takes a config array and creates a client instance
     */
    public function createFrom(array $config): Client
    {
        if ($config['type'] === 'generic') {
            $providerConfig = new GenericOAuthProviderConfig(
                $config['clientId'],
                $config['clientSecret'],
                $config['redirectUri'],
                $config['authorizeEndpoint'],
                $config['tokenEndpoint'],
                $config['scopes'] ?? [],
            );
            return new GenericOAuthClient($providerConfig);
        }

        if ($config['type'] === 'google') {
            return new GoogleConnectClient($config);
        }

        if ($config['type'] === 'microsoft') {
            return new MicrosoftConnectClient($config);
        }

        throw new \LogicException('Unknown provider type');
    }
}
