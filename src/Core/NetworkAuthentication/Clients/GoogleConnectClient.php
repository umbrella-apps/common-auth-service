<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication\Clients;

use App\Core\NetworkAuthentication\OAuth2Client;
use App\Core\NetworkAuthentication\OpenIdUserInfoUser;
use App\Core\NetworkAuthentication\ExternalUser ;
use League\OAuth2\Client\Provider\Google;
use League\OAuth2\Client\Token\AccessToken;

class GoogleConnectClient implements OAuth2Client
{
    private $provider;

    public function __construct(array $config)
    {
        $this->provider = new Google($config);
    }

    public function getAuthorizationUrl(): string
    {
        return $this->provider->getAuthorizationUrl();
    }

    public function getAccessToken($grant, array $options = [])
    {
        return $this->provider->getAccessToken($grant, $options);
    }

    public function getUserDetailsFor(AccessToken $token): ExternalUser
    {
        $resourceOwner = $this->provider->getResourceOwner($token);
        return new OpenIdUserInfoUser('google', $resourceOwner->toArray());
    }
}
