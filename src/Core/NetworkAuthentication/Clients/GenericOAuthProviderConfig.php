<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication\Clients;

class GenericOAuthProviderConfig
{
    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $clientSecret;

    /**
     * @var string
     */
    private $redirectUri;

    /**
     * The endpoint for OAuth authorization
     * @var string
     */
    private $authorizeEndpoint;

    /**
     * The endpoint of the OAuth Server to request tokens
     * @var string
     */
    private $tokenEndpoint;

    /**
     * The scopes to request
     * @var string[]
     */
    private $scopes = [];

    /**
     * GenericOAuthProviderConfig constructor.
     * @param string $clientId
     * @param string $clientSecret
     * @param string $redirectUri
     * @param string $authorizeEndpoint
     * @param string $tokenEndpoint
     * @param string[] $scopes
     */
    public function __construct(
        string $clientId,
        string $clientSecret,
        string $redirectUri,
        string $authorizeEndpoint,
        string $tokenEndpoint,
        array $scopes = []
    ) {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->redirectUri = $redirectUri;
        $this->authorizeEndpoint = $authorizeEndpoint;
        $this->tokenEndpoint = $tokenEndpoint;
        $this->scopes = $scopes;
    }

    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    public function getRedirectUri(): string
    {
        return $this->redirectUri;
    }

    public function getAuthorizeEndpoint(): string
    {
        return $this->authorizeEndpoint;
    }

    public function getTokenEndpoint(): string
    {
        return $this->tokenEndpoint;
    }

    public function getScopes(): array
    {
        return $this->scopes;
    }

    public function toArray(): array
    {
        return [
            'clientId' => $this->clientId,
            'clientSecret' => $this->clientSecret,
            'redirectUri' => $this->redirectUri,
        ];
    }
}
