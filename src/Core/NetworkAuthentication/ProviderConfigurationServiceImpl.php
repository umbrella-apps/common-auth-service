<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

use App\Core\NetworkAuthentication\Domain\ProviderReference;
use App\Core\NetworkAuthentication\Exception\UnknownProvider;
use App\Core\Organisation\OrganisationAuthenticationProvider;
use App\Core\Organisation\OrganisationRepository;

/**
 * Makes available the different clients for all configured providers
 */
class ProviderConfigurationServiceImpl implements ProviderConfigurationService
{
    /**
     * @var array
     */
    private $globalProviderConfig;

    /**
     * @var OrganisationRepository
     */
    private $organisationRepository;

    public function __construct(
        $appExternalLoginGlobalProvider,
        OrganisationRepository $organisationRepository
    ) {
        $this->globalProviderConfig = $appExternalLoginGlobalProvider;
        $this->organisationRepository = $organisationRepository;
    }

    public function fromReference(ProviderReference $reference): array
    {
        if ($reference->isGlobal()) {
            return $this->getGlobalProviderDefinition($reference->getKeyOrId());
        }

        $providerDefinition = $this->getProviderDefinition($reference->getKeyOrId());
        if ($providerDefinition->referencesGlobalProvider()) {
            // this provider actually just enables/aliases a global provider
            $providerKey = $providerDefinition
                ->getConfiguration()[OrganisationAuthenticationProvider::CONFIG_REFERENCE];
            return $this->getGlobalProviderDefinition($providerKey);
        }
        return $this->clientConfigurationFrom($providerDefinition);
    }

    private function getProviderDefinition(
        string $providerId
    ): OrganisationAuthenticationProvider {
        $providerDefinition = $this->organisationRepository->providerWithId($providerId);
        ;

        if (null === $providerDefinition) {
            throw UnknownProvider::withId($providerId);
        }

        if ($providerDefinition->referencesParentProvider()) {
            // this provider actually just enables/aliases a parents provider
            $providerKey = $providerDefinition
                ->getConfiguration()[OrganisationAuthenticationProvider::CONFIG_REFERENCE];
            $this->getProviderDefinition($providerKey);
        }

        return $providerDefinition;
    }

    public function getGlobalProviderDefinition(string $providerKey): array
    {
        if (!array_key_exists($providerKey, $this->globalProviderConfig)) {
            throw UnknownProvider::globalWithId($providerKey);
        }

        return $this->globalProviderConfig[$providerKey];
    }

    private function clientConfigurationFrom(
        OrganisationAuthenticationProvider $providerDefinition
    ): array {
        $config = $providerDefinition->getConfiguration();
        $config['type'] = $providerDefinition->getType();
        return $config;
    }
}
