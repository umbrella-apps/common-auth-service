<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\NetworkAuthentication;

use App\Core\NetworkAuthentication\Domain\ProviderReference;

interface ProviderConfigurationService
{
    public function fromReference(ProviderReference $reference): array;

    public function getGlobalProviderDefinition(string $providerKey): array;
}
