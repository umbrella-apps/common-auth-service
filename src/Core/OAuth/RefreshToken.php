<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\OAuth;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="oauth2_refreshtoken")
 */
class RefreshToken
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", length=80)
     */
    private $id;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $expiryDateTime;

    /**
     * @var string
     * @ORM\Column(type="string", length=80)
     */
    private $accessTokenId;

    public function __construct(
        string $id,
        DateTimeImmutable $expiryDateTime,
        string $accessTokenId
    ) {
        $this->id = $id;
        $this->expiryDateTime = $expiryDateTime;
        $this->accessTokenId = $accessTokenId;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function expiryDateTime(): \DateTimeImmutable
    {
        return $this->expiryDateTime;
    }

    public function accessToken(): string
    {
        return $this->accessTokenId;
    }
}
