<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\OAuth;

use Doctrine\ORM\EntityManagerInterface;

final class AccessTokenRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function withId(string $tokenId): ?AccessToken
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->em->getRepository(AccessToken::class)->find($tokenId);
    }

    public function save(AccessToken $token)
    {
        $this->em->persist($token);
        $this->em->flush();
    }

    public function delete(AccessToken $token)
    {
        $this->em->remove($token);
        $this->em->flush();
    }
}
