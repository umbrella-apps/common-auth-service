<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\OAuth;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="oauth2_accesstoken")
 */
class AccessToken
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", length=80)
     */
    private $id;

    /**
     * @var DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $expiryDateTime;

    /**
     * @var string
     * @ORM\Column(type="guid")
     */
    private $client;

    /**
     * @var string[]
     * @ORM\Column(type="json")
     */
    private $scopes;

    /**
     * @var string|null
     * @ORM\Column(type="guid", nullable=true)
     */
    private $userId;

    public function __construct(
        string $id,
        DateTimeImmutable $expiryDateTime,
        string $client,
        array $scopes,
        string $userId = null
    ) {
        $this->id = $id;
        $this->expiryDateTime = $expiryDateTime;
        $this->client = $client;
        $this->scopes = $scopes;
        $this->userId = $userId;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function expiryDateTime(): DateTimeImmutable
    {
        return $this->expiryDateTime;
    }

    public function user(): string
    {
        return $this->userId ?? 'CLIENT-' . $this->client;
    }

    public function client(): string
    {
        return $this->client;
    }

    public function scopes(): array
    {
        return $this->scopes;
    }
}
