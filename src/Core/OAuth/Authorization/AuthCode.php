<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\OAuth\Authorization;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="oauth2_authcode")
 */
class AuthCode
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=80)
     */
    private $id;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $expiryDateTime;

    /**
     * @var string
     * @ORM\Column(type="guid")
     */
    private $client;

    /**
     * @var string[]
     * @ORM\Column(type="json")
     */
    private $scopes;

    /**
     * @var string
     * @ORM\Column(type="guid")
     */
    private $user;

    public function __construct(
        string $id,
        DateTimeImmutable $expiryDateTime,
        string $client,
        array $scopes,
        string $user
    ) {
        $this->id = $id;
        $this->expiryDateTime = $expiryDateTime;
        $this->client = $client;
        $this->scopes = $scopes;
        $this->user = $user;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function user(): string
    {
        return $this->user;
    }

    public function expiryDateTime(): DateTimeImmutable
    {
        return $this->expiryDateTime;
    }

    public function client(): string
    {
        return $this->client;
    }

    public function getScopes(): array
    {
        return $this->scopes;
    }
}
