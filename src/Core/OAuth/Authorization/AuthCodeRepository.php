<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\OAuth\Authorization;

use Doctrine\ORM\EntityManagerInterface;

class AuthCodeRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function has(string $code): bool
    {
        return $this->em->getRepository(AuthCode::class)->find($code) !== null;
    }

    public function add(AuthCode $code)
    {
        $this->em->persist($code);
        $this->em->flush();
    }

    public function revokeWithId(string $code): void
    {
        /** @var AuthCode $codeObj */
        $codeObj = $this->em->getRepository(AuthCode::class)->find($code);
        if ($codeObj) {
            $this->revoke($codeObj);
        }
    }

    public function revoke(AuthCode $code)
    {
        $this->em->remove($code);
        $this->em->flush();
    }
}
