<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Core\OAuth;

use App\Core\User\User;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface OAuth2Service
{
    public function validateAuthorizationRequest(ServerRequestInterface $request): AuthorizationRequest;

    public function completeAuthorizationRequest(AuthorizationRequest $authRequest, ResponseInterface $response): ResponseInterface;

    public function respondToAccessTokenRequest(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface;

    public function validateClient(ServerRequestInterface $request, User $user): bool;
}
