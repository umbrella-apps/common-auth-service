<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Controller\API;

use App\Core\Organisation\OrganisationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class OrganisationController extends AbstractController
{
    /**
     * @Route("/api/organisations", name="api_organisations")
     * @param OrganisationRepository $repo
     * @return JsonResponse
     */
    public function index(OrganisationRepository $repo)
    {
        $this->denyAccessUnlessGranted('organisation.read-only');
        return $this->json($repo->all());
    }
}
