<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Core\NetworkAuthentication\ProviderClientServiceImpl;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConnectController extends AbstractController
{
    /**
     * @var ProviderClientServiceImpl
     */
    private $providerClientService;

    public function __construct(ProviderClientServiceImpl $providerClientService)
    {
        $this->providerClientService = $providerClientService;
    }

    /**
     * @Route("/connect/g/{providerKey}", name="connect_global")
     */
    public function connectWithGlobalAction(string $providerKey): Response
    {
        $client = $this->providerClientService->getGlobalClient($providerKey);
        return new RedirectResponse($client->getAuthorizationUrl());
    }

    /**
     * @Route("/connect/{providerId}", name="connect")
     */
    public function connectAction(string $providerId): Response
    {
        $client = $this->providerClientService->getClient($providerId);
        return new RedirectResponse($client->getAuthorizationUrl());
    }

    /**
     * The actual checking happens in the Authenticator
     * @Route("/connect/g/{providerKey}/check", name="connect_global_check")
     */
    public function connectWithGlobalCheckAction(): Response
    {
        return new RedirectResponse('/');
    }

    /**
     * The actual checking happens in the Authenticator
     * @Route("/connect/{providerId}/check", name="connect_check")
     */
    public function connectCheckAction(): Response
    {
        return new RedirectResponse('/');
    }
}
