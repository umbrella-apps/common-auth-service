<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Core\Organisation\Organisation;
use App\Core\User\User;
use App\Form\OrganisationType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/admin/organisations")
 */
class OrganisationController extends AbstractController
{


    /**
     * @Route("/", name="organisation_index", methods={"GET"})
     */
    public function index(UserInterface $user): Response
    {
        if (!($user instanceof User) || !$user->getOrganisation()) {
            throw new \LogicException('this should not happen');
        }

        $topOrganisation = $user->getOrganisation();
        $organisations = [$topOrganisation, ...$topOrganisation->getAllChildOrganisations()];

        return $this->render(
            'admin/organisations/index.html.twig',
            [
                'organisations' => $organisations,
            ]
        );
    }

    /**
     * @Route("/new", name="organisation_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $organisation = new Organisation();
        $form = $this->createForm(OrganisationType::class, $organisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($organisation);
            $entityManager->flush();

            return $this->redirectToRoute('organisation_index');
        }

        return $this->render(
            'admin/organisations/new.html.twig',
            [
                'organisation' => $organisation,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="organisation_show", methods={"GET"})
     */
    public function show(Organisation $organisation): Response
    {
        return $this->render(
            'admin/organisations/show.html.twig',
            [
                'organisation' => $organisation,
            ]
        );
    }

    /**
     * @Route("/{id}/edit", name="organisation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Organisation $organisation): Response
    {
        $form = $this->createForm(OrganisationType::class, $organisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('organisation_index');
        }

        return $this->render(
            'admin/organisations/edit.html.twig',
            [
                'organisation' => $organisation,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="organisation_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Organisation $organisation): Response
    {
        if ($this->isCsrfTokenValid('delete' . $organisation->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($organisation);
            $entityManager->flush();
        }

        return $this->redirectToRoute('organisation_index');
    }
}
