<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Core\Organisation\OrganisationRepository;
use App\Core\Shared\Domain;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(
        AuthenticationUtils $authenticationUtils,
        array $appExternalLoginGlobalProvider = []
    ): Response {
        if ($this->getUser()) {
            // user was already signed in
            return $this->redirectToRoute('home');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'security/login.html.twig',
            [
                'last_username' => $lastUsername,
                'error' => $error,
                'global_provider' => $appExternalLoginGlobalProvider
            ]
        );
    }

    /**
     * @Route("/login/discover", name="app_login_discover")
     */
    public function discover(
        Request $request,
        OrganisationRepository $organisationRepository,
        $appExternalLoginGlobalProvider
    ): Response {
        $organisation = null;
        $domain = null;
        if ($request->query->has('organisation')) {
            $id = $request->query->get('organisation');
            $organisation = $organisationRepository->withId($id);
        } elseif ($request->query->has('domain')) {
            $domainOrEmailStr = $request->query->get('domain');
            $domain = str_contains($domainOrEmailStr, '@') ? Domain::fromEmail($domainOrEmailStr) : Domain::fromString(
                $domainOrEmailStr
            );
            $organisation = $organisationRepository->withDomain($domain);
        }

        return $this->render(
            'security/discover.html.twig',
            [
                'domain' => $domain,
                'organisation' => $organisation,
                'globalProviderConfig' => $appExternalLoginGlobalProvider,
                're' => $request->query->get('re')
            ]
        );
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException(
            'This method can be blank - it will be intercepted by the logout key on your firewall.'
        );
    }
}
