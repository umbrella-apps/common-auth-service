<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Core\User\User;
use App\Core\OAuth\OAuth2Service;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Makes available the OAuth2 endpoints needed by clients for authorization
 */
class OAuthController extends AbstractController
{
    /**
     * @var OAuth2Service
     */
    private $oAuth2Service;

    public function __construct(OAuth2Service $oAuth2Service)
    {
        $this->oAuth2Service = $oAuth2Service;
    }

    /**
     * Authorize endpoint for auth code grant
     *
     * @Route("/oauth2/authorize", name="oauth2_authorize")
     */
    public function authorizeAction(ServerRequestInterface $request): ResponseInterface
    {
        // Query Params: response_type (code), client_id, redirect_uri, scope, state,
        $authRequest = $this->oAuth2Service->validateAuthorizationRequest($request);

        /** @var User $user */
        $user = $this->getUser();
        if ($user === null) {
            throw new \LogicException('This route should not be accessible without a user being logged in.');
        }

        if (!$this->oAuth2Service->validateClient($request, $user)) {
            throw new BadRequestException('This client is not enabled for this user.');
        }

        $authRequest->setUser($user);
        $authRequest->setAuthorizationApproved(true);

        return $this->oAuth2Service->completeAuthorizationRequest($authRequest, new Response());
    }

    /**
     * Endpoint for requesting OAuth2 tokens
     *
     * @Route("/oauth2/token", name="oauth2_token")
     */
    public function tokenAction(ServerRequestInterface $request): ResponseInterface
    {
        return $this->oAuth2Service->respondToAccessTokenRequest($request, new Response());
    }
}
