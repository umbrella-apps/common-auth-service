<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Core\NetworkAuthentication\Domain\ProviderReference;
use App\Core\Organisation\Organisation;
use App\Core\Organisation\OrganisationRepository;
use App\Core\Shared\Domain;
use App\Core\User\User;
use App\Form\RegistrationFormType;
use App\Security\EmailVerifier;
use App\Security\DirectLoginAuthenticator;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Message;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegistrationController extends AbstractController
{
    /**
     * @var EmailVerifier
     */
    private $emailVerifier;

    /**
     * @var OrganisationRepository
     */
    private $organisationRepo;

    /**
     * @var string[]
     */
    private $appEmail;

    public function __construct(
        EmailVerifier $emailVerifier,
        OrganisationRepository $organisationRepo,
        array $appEmail
    ) {
        $this->emailVerifier = $emailVerifier;
        $this->organisationRepo = $organisationRepo;
        $this->appEmail = $appEmail;
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        DirectLoginAuthenticator $authenticator
    ): Response {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $domain = Domain::fromEmail($user->getEmail());
            $organisation = $this->organisationRepo->withDomain($domain);

            if ($organisation === null) {
                return $this->redirectToRoute('app_register_organisation');
            }

            if (!$organisation->supportsAuthenticationThrough(new ProviderReference(true, 'local'), $domain)) {
                return $this->redirectToRoute('app_login_discover', ['domain' => $user->getEmail(), 're' => 'localNotSupported']);
            }

            $user->setOrganisation($organisation);

            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setCreatedAt(new \DateTimeImmutable());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation(
                'app_verify_email',
                $user,
                (new TemplatedEmail())
                    ->from($this->getFromAddress())
                    ->to($user->getEmail())
                    ->subject('Please confirm your email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );
            // do anything else you need here, like send an email

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render(
            'registration/register.html.twig',
            [
                'registrationForm' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/verify/email", name="app_verify_email")
     */
    public function verifyUserEmail(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());
            return $this->redirectToRoute('app_register');
        }

        return $this->redirectToRoute('home');
    }


    /**
     * @Route("/verify/email/request", name="app_verify_email_request")
     */
    public function sendUserVerificationEmail(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // todo configure email
        $this->emailVerifier->sendEmailConfirmation(
            'app_verify_email',
            $this->getUser(),
            (new TemplatedEmail())
                ->from(new Address('accounts@example.com', 'Account Management'))
                ->to($this->getUser()->getEmail())
                ->subject('Please confirm your email')
                ->htmlTemplate('registration/confirmation_email.html.twig')
        );
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/register/organisation", name="app_register_organisation")
     */
    public function registerOrganisation(): Response
    {
        return $this->render(
            'registration/register-organisation.html.twig'
        );
    }

    private function getFromAddress(): Address
    {
        return new Address($this->appEmail['from'], $this->appEmail['from_name']);
    }
}
