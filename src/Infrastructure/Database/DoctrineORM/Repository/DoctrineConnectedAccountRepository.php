<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Database\DoctrineORM\Repository;

use App\Core\NetworkAuthentication\ConnectedAccount;
use App\Core\NetworkAuthentication\ConnectedAccountRepository;
use App\Core\NetworkAuthentication\Domain\ProviderReference;
use App\Core\NetworkAuthentication\ExternalUser;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

final class DoctrineConnectedAccountRepository implements ConnectedAccountRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ObjectRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $this->em->getRepository(ConnectedAccount::class);
    }

    public function all(): array
    {
        return $this->repo->findAll();
    }

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    public function getFor(
        ExternalUser $externalUser,
        ProviderReference $provider
    ): ?ConnectedAccount {
        return $this->repo->findOneBy(
            [
                'externalId' => $externalUser->id(),
                'provider.isGlobal' => $provider->isGlobal(),
                'provider.keyOrId' => $provider->getKeyOrId()
            ]
        );
    }
}
