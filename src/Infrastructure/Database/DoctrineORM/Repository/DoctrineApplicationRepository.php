<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Database\DoctrineORM\Repository;

use App\Core\Application\Application;
use App\Core\Application\ApplicationRepository;
use App\Core\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

final class DoctrineApplicationRepository implements ApplicationRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ObjectRepository
     */
    private $repo;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repo = $this->em->getRepository(Application::class);
    }

    /**
     * @return Application[]
     */
    public function all(): array
    {
        return $this->repo->findAll();
    }

    public function withId(string $id): ?Application
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->repo->find($id);
    }

    /**
     * @return Application[]
     */
    public function allFor(User $user): array
    {
        return $this->repo->findBy(['organisation' => $user->getOrganisation()->getId()]);
    }
}
