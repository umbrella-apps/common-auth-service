<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Database\DoctrineORM\Repository;

use App\Core\Organisation\OrganisationAuthenticationProvider;
use App\Core\Organisation\OrganisationRepository;
use App\Core\Shared\Domain;
use App\Core\Organisation\Organisation;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class DoctrineOrganisationRepository implements OrganisationRepository
{
    private const ENTITY = Organisation::class;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @inheritDoc
     */
    public function all(): array
    {
        /** @var ObjectRepository $repo */
        $repo = $this->em->getRepository(self::ENTITY);
        return $repo->findAll();
    }

    /**
     * @inheritDoc
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    public function withId(string $id): ?Organisation
    {
        $repo = $this->em->getRepository(self::ENTITY);
        return $repo->find($id);
    }

    public function withDomain(Domain $domain): ?Organisation
    {
        $qb = $this->em->createQueryBuilder();
        return $qb
            ->select('o')
            ->from(self::ENTITY, 'o')
            ->leftJoin('o.providers', 'p')
            ->where($qb->expr()->eq('p.domain', ':domain'))
            ->setParameter('domain', $domain->value())
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function persist(Organisation $organisation): void
    {
        $this->em->persist($organisation);
        $this->em->flush();
    }

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    public function providerWithId(string $providerId): ?OrganisationAuthenticationProvider
    {
        return $this->em->getRepository(OrganisationAuthenticationProvider::class)->find($providerId);
    }
}
