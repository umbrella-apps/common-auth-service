<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Database\DoctrineORM\Repository;

use App\Core\User\User;
use App\Core\User\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

final class DoctrineUserRepository implements UserRepository
{
    private const ENTITY = User::class;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @inheritDoc
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    public function withId(string $id): ?User
    {
        $repo = $this->em->getRepository(self::ENTITY);
        return $repo->find($id);
    }

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    public function withEmail(?string $email): ?User
    {
        $repo = $this->em->getRepository(self::ENTITY);
        return $repo->findOneBy(['email' => $email]);
    }

    public function save(User $user): void
    {
        $this->em->persist($user);
        $this->em->flush();
    }

    public function delete(User $user): void
    {
        $this->em->remove($user);
        $this->em->flush();
    }
}
