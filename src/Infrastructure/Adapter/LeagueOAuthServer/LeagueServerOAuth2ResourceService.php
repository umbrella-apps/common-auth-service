<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Adapter\LeagueOAuthServer;

use App\Core\OAuth\OAuth2ResourceService;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\ResourceServer;
use Psr\Http\Message\ServerRequestInterface;

class LeagueServerOAuth2ResourceService implements OAuth2ResourceService
{
    /**
     * @var ResourceServer
     */
    private $resourceServer;

    public function __construct(
        AccessTokenRepositoryInterface $accessTokenRepository,
        string $publicKeyPath
    ) {
        $this->resourceServer = new ResourceServer(
            $accessTokenRepository,
            new CryptKey($publicKeyPath, null, false)
        );
    }

    public function validateAuthenticatedRequest(
        ServerRequestInterface $request
    ): ServerRequestInterface {
        return $this->resourceServer->validateAuthenticatedRequest(
            $request
        );
    }
}
