<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Adapter\LeagueOAuthServer;

use App\Core\Application\ApplicationRepository;
use App\Core\OAuth\ScopeService;
use App\Core\User\UserRepository;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;

class LeagueScopeRepository implements ScopeRepositoryInterface
{
    /**
     * @var ScopeService
     */
    private $scopeService;

    /**
     * @var ApplicationRepository
     */
    private $clientRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(
        ScopeService $scopeService,
        ApplicationRepository $clientRepository,
        UserRepository $userRepository
    ) {
        $this->scopeService = $scopeService;
        $this->clientRepository = $clientRepository;
        $this->userRepository = $userRepository;
    }

    public function getScopeEntityByIdentifier($identifier): ?ScopeEntityInterface
    {
        if (!$this->scopeService->isValidScope($identifier)) {
            return null;
        }
        return new LeagueScope($identifier);
    }

    /**
     * This method is called right before an access token or authorization code is created.
     *
     * Given a client, grant type and optional user identifier validate the set of scopes requested are valid and optionally append additional scopes or remove requested scopes.
     * This method is useful for integrating with your own app’s permissions system.
     *
     * You must return an array of ScopeEntityInterface instances; either the original scopes or an updated set.
     */
    public function finalizeScopes(
        array $scopes,
        $grantType,
        ClientEntityInterface $clientEntity,
        $userIdentifier = null
    ): array {
        $scopes = array_map(fn($s) => $s->getIdentifier(), $scopes);
        $client = $this->clientRepository->withId($clientEntity->getIdentifier());
        $user = $userIdentifier != null ? $this->userRepository->withId($userIdentifier) : null;

        $scopes = $this->scopeService->matchScopes($scopes, $client, $user);

        return array_map(fn($s) => new LeagueScope($s), $scopes);
    }
}
