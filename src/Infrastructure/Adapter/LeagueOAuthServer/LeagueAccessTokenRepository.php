<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Adapter\LeagueOAuthServer;

use App\Core\OAuth\AccessToken;
use App\Core\OAuth\AccessTokenRepository;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;

final class LeagueAccessTokenRepository implements AccessTokenRepositoryInterface
{
    /**
     * @var AccessTokenRepository
     */
    private $tokenRepository;

    public function __construct(AccessTokenRepository $tokenRepository)
    {
        $this->tokenRepository = $tokenRepository;
    }

    public function getNewToken(
        ClientEntityInterface $client,
        array $scopes,
        $userIdentifier = null
    ): AccessTokenEntityInterface {
        $token = new LeagueAccessTokenEntity();
        $token->setClient($client);
        $token->setUserIdentifier($userIdentifier);
        foreach ($scopes as $scope) {
            $token->addScope($scope);
        }
        return $token;
    }

    public function persistNewAccessToken(AccessTokenEntityInterface $accessToken): void
    {
        $scopes = array_map(fn($s) => $s->getIdentifier(), $accessToken->getScopes());
        $token = new AccessToken(
            $accessToken->getIdentifier(),
            $accessToken->getExpiryDateTime(),
            $accessToken->getClient()->getIdentifier(),
            $scopes
        );
        $this->tokenRepository->save($token);
    }

    public function revokeAccessToken($tokenId): void
    {
        $token = $this->tokenRepository->withId($tokenId);
        if ($token === null) {
            return;
        }
        $this->tokenRepository->delete($token);
    }

    public function isAccessTokenRevoked($tokenId): bool
    {
        $token = $this->tokenRepository->withId($tokenId);
        if ($token === null) {
            return true;
        }
        return false;
    }
}
