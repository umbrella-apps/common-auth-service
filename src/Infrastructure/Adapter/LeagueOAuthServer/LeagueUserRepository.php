<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Adapter\LeagueOAuthServer;

use App\Core\User\UserRepository;
// use League\OAuth2\Server\Repositories\UserRepositoryInterface as LeagueUserRepositoryInterface;
use OpenIDConnectServer\Repositories\IdentityProviderInterface;

final class LeagueUserRepository implements
    // LeagueUserRepositoryInterface,
    IdentityProviderInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUserEntityByIdentifier($identifier): LeagueUserEntity
    {
        $user = $this->userRepository->withId($identifier);
        return LeagueUserEntity::fromUser($user);
    }
}
