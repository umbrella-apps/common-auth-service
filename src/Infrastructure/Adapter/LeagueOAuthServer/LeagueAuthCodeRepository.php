<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Adapter\LeagueOAuthServer;

use App\Core\OAuth\Authorization\AuthCode;
use App\Core\OAuth\Authorization\AuthCodeRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use League\OAuth2\Server\Entities\AuthCodeEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;

final class LeagueAuthCodeRepository implements AuthCodeRepositoryInterface
{
    /**
     * @var AuthCodeRepository
     */
    private $codeRepository;

    public function __construct(AuthCodeRepository $codeRepository)
    {
        $this->codeRepository = $codeRepository;
    }

    public function getNewAuthCode(): AuthCodeEntityInterface
    {
        return new LeagueAuthCode();
    }

    public function persistNewAuthCode(AuthCodeEntityInterface $authCodeEntity): void
    {
        try {
            $scopes = array_map(fn($s) => $s->getIdentifier(), $authCodeEntity->getScopes());
            $code = new AuthCode(
                $authCodeEntity->getIdentifier(),
                $authCodeEntity->getExpiryDateTime(),
                $authCodeEntity->getClient()->getIdentifier(),
                $scopes,
                $authCodeEntity->getUserIdentifier()
            );
            $this->codeRepository->add($code);
        } catch (UniqueConstraintViolationException $e) {
            // TODO can be thrown by doctrine, there should probably be a core exception
            throw UniqueTokenIdentifierConstraintViolationException::create();
        }
    }

    public function revokeAuthCode($codeId): void
    {
        $this->codeRepository->revokeWithId($codeId);
    }

    public function isAuthCodeRevoked($codeId)
    {
        return !$this->codeRepository->has($codeId);
    }
}
