<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Adapter\LeagueOAuthServer;

use App\Core\Application\ApplicationRepository;
use App\Core\OAuth\OAuth2Service;
use App\Core\User\User;
use Defuse\Crypto\Key;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\Grant\AuthCodeGrant;
use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;
use OpenIDConnectServer\ClaimExtractor;
use OpenIDConnectServer\IdTokenResponse;
use OpenIDConnectServer\Repositories\IdentityProviderInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class LeagueServerOAuth2Service implements OAuth2Service
{
    /**
     * @var AuthorizationServer
     */
    private $authorizationServer;

    /**
     * @var ApplicationRepository
     */
    private $clientRepository;

    public function __construct(
        ApplicationRepository $appClientRepository,
        ClientRepositoryInterface $clientRepository,
        AccessTokenRepositoryInterface $accessTokenRepository,
        ScopeRepositoryInterface $scopeRepository,
        AuthCodeRepositoryInterface $authCodeRepository,
        RefreshTokenRepositoryInterface $refreshTokenRepository,
        IdentityProviderInterface $userRepo,
        string $privateKeyPath,
        string $encryptionKey,
        string $authCodeTTL = 'PT10M'
    ) {
        $authCodeTTL = new \DateInterval($authCodeTTL);

        // OpenID Connect Response Type
        $responseType = new IdTokenResponse(
            $userRepo,
            new ClaimExtractor()
        );

        $this->authorizationServer = new AuthorizationServer(
            $clientRepository,
            $accessTokenRepository,
            $scopeRepository,
            new CryptKey($privateKeyPath, null, false),
            Key::loadFromAsciiSafeString($encryptionKey),
            $responseType
        );

        $authCodeGrant = new AuthCodeGrant($authCodeRepository, $refreshTokenRepository, $authCodeTTL);
        $this->authorizationServer->enableGrantType($authCodeGrant);

        $clientCredGrant = new ClientCredentialsGrant();
        $this->authorizationServer->enableGrantType($clientCredGrant);

        $refreshTokenGrant = new RefreshTokenGrant($refreshTokenRepository);
        $this->authorizationServer->enableGrantType($refreshTokenGrant);

        $this->clientRepository = $appClientRepository;
    }

    public function validateAuthorizationRequest(ServerRequestInterface $request): AuthorizationRequest
    {
        return $this->authorizationServer->validateAuthorizationRequest(
            $request
        );
    }

    public function completeAuthorizationRequest(AuthorizationRequest $authRequest, ResponseInterface $response): ResponseInterface
    {
        return $this->authorizationServer->completeAuthorizationRequest(
            $authRequest,
            $response
        );
    }

    public function respondToAccessTokenRequest(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        return $this->authorizationServer->respondToAccessTokenRequest(
            $request,
            $response
        );
    }

    public function validateClient(ServerRequestInterface $request, User $user): bool
    {
        $clientId = $request->getQueryParams()['client_id'];
        $client = $this->clientRepository->withId($clientId);
        return $client->hasAccess($user->getOrganisation());
    }
}
