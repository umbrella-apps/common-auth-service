<?php

/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace App\Infrastructure\Adapter\LeagueOAuthServer;

use App\Core\User\User;
use League\OAuth2\Server\Entities\UserEntityInterface;
use OpenIDConnectServer\Entities\ClaimSetInterface;

final class LeagueUserEntity implements UserEntityInterface, ClaimSetInterface
{
    /**
     * @var string
     */
    private $identifier;

    protected $attributes;

    public static function fromUser(User $user)
    {
        $attributes =  [
            // profile (incomplete)
            'name' => $user->getName(),
            'family_name' => $user->getFamilyName(),
            'given_name' => $user->getGivenName(),
            'preferred_username' => $user->getUsername(),
            'updated_at' => $user->getUpdatedAt()->getTimestamp(),
            // email
            'email' => $user->getEmail(),
            'email_verified' => $user->isVerified(),
        ];
        return new self((string)$user->getId(), $attributes);
    }

    private function __construct(string $id, array $attributes = [])
    {
        $this->identifier = $id;
        $this->attributes = $attributes;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getClaims()
    {
        return $this->attributes;
    }
}
