/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Controller } from 'stimulus';

export default class extends Controller {
    static targets = [ "menu", "iconOpen", "iconClose" ];
    _open = false;

    toggle() {
        console.log('toggle');
        if (this._open) {
            return this.close();
        }
        this.open();
    }

    open() {
        this._open = true;
        this._show(this.menuTarget);
        this._show(this.iconCloseTarget);
        this._hide(this.iconOpenTarget);
    }

    close() {
        this._open = false;
        this._hide(this.menuTarget);
        this._hide(this.iconCloseTarget);
        this._show(this.iconOpenTarget);
    }

    _hide(element) {
        element.classList.remove('block')
        element.classList.add('hidden')
    }

    _show(element) {
        element.classList.add('block')
        element.classList.remove('hidden')
    }
}
