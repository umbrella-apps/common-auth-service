/*
 * (c) Julius Stoerrle <juliusstoerrle@gmx.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import './styles/app.scss';

// start the Stimulus application
import './bootstrap';

import '../user-config/theme.scss';